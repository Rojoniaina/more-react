module.exports = function(app) {
  
  var moduleTasks = require('../controller/taskController.js');

  // todoList Routes
  app.route('/tasks')
    .get(moduleTasks.list_all_tasks)
    .post(moduleTasks.create_a_task);
   
   app.route('/tasks/:taskId')
    .get(moduleTasks.read_a_task)
    .put(moduleTasks.update_a_task)
    .delete(moduleTasks.delete_a_task);

};