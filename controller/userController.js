var User = require('../model/userModel.js');

exports.list_all_users = function(req, res) {
  User.getAllUsers(function(err, user) {
    if (err)
      res.send(err);
      
    res.send(user);
  });
};

exports.read_a_user = function(req, res) {
  User.getUserById(req.params.u_id, function(err, user) {
    if (err)
      res.send(err);

    res.json(user);
  });
};

exports.create_a_user = function(req, res) {
  console.log(req.body);
  var new_user = new User(req.body);
  if(!new_user.u_name){
    res.status(400).send({ error:true, message: 'Please provide user' });
  }
  else{
  User.createTask(new_user, function(err, user) {
    
    if (err)
      res.send(err);

    res.json(user);
  });
}
};


exports.update_a_task = function(req, res) {
  User.updateById(req.params.u_id, new User(req.body), function(err, user) {
    if (err)
      res.send(err);

    res.json(user);
  });
};


exports.delete_a_task = function(req, res) {
  User.remove( req.params.u_id, function(err, user) {
    if (err)
      res.send(err);
    
    res.json({ message: 'User successfully deleted' });
  });
};