const express 		= require('express');
const bodyParser 	= require('body-parser');
// const path 			= require('path');

const app 	= express();
const port 	= process.env.PORT || 5000;

app.listen(port);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// importing route
var routes = require('./routes/approutes'); 

// register the route
routes(app);