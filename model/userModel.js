var sql = require('./db.js');

//Task object constructor
var User = function(user){
    this.u_id       = user.u_id;
    this.u_name     = user.u_name;
    this.u_email    = user.u_email;
    this.u_adresse  = user.u_address;
};

User.getAllUsers = function getAllUsers(result) {
    sql.query("Select * from users", function (err, res) {
        if(err) {
            result(null, err);
        }
        else{
            result(null, res);
        }
    });   
};

User.getUserById = function getUserById(userId, result) {
    sql.query("Select * from users where id = ? ", taskId, function (err, res) {             
        if(err) {
            result(err, null);
        }
        else{
            result(null, res);
        }
    });   
};

User.createUser = function createUser(newUser, result) {    
    sql.query("INSERT INTO users set ?", newUser, function (err, res) {
        if(err) {
            result(err, null);
        }
        else{
            result(null, res.insertId);
        }
    });           
};

User.updateUserById = function(id, task, result){
    sql.query("UPDATE users SET task = ? WHERE id = ?", [task.task, id], function (err, res) {
        if(err) {
            result(null, err);
        }
        else{   
            result(null, res);
            }
    }); 
};

User.removeUser = function(id, result){
    sql.query("DELETE FROM users WHERE id = ?", [id], function (err, res) {
        if(err) {
            result(null, err);
        }
        else{
         result(null, res);
        }
    }); 
};

module.exports = User;