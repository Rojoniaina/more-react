import React, { Component } from 'react';
import Taskes from './Taskes';
import Users  from './Users';
// import axios from 'axios';

class App extends Component {

  state = 
  {
    menu : { ishome  : 'active', istache : '', isuser  : '' },
  };

  constructor(props) {
      super(props);
      this.onMenuClick = this.onMenuClick.bind(this);
  }
  
  onMenuClick(event){
    event.preventDefault();
    const t = event.target;
    const ahome = (t.id === 'ishome')  ? 'active' : '';
    const atask = (t.id === 'istache') ? 'active' : '';
    const auser = (t.id === 'isuser')  ? 'active' : '';
    this.setState({ menu : { ishome  : ahome, istache : atask, isuser  : auser}, });
  }
  

  render() {
      let contenu = <Home />;
      if(this.state.menu.istache === 'active') contenu = <Taskes />;
      if(this.state.menu.isuser === 'active')  contenu = <Users />;
      return (
              <div>
                <Menu 
                  menu   = {this.state.menu}
                  onMenu = {this.onMenuClick}
                />
                
                {contenu}
              </div>
              );
    }
  }

  const Menu = function(props) {
    return (
              <div className="header clearfix">
                <nav>
                  <ul className="nav nav-pills pull-right">
                    <li role="presentation" className={props.menu.ishome}>
                      <a href="#home" id="ishome" onClick={props.onMenu}>Accueil</a>
                    </li>
                    <li role="presentation" className={props.menu.istache}>
                      <a href="#Taskes" id="istache" onClick={props.onMenu}>Taches</a>
                    </li>
                    <li role="presentation" className={props.menu.isuser}>
                      <a href="#users" id="isuser" onClick={props.onMenu}>Utilisateurs</a>
                    </li>
                  </ul>
                </nav>
                <h3 className="text-muted">More React</h3>
              </div>
            )
  }

  const Home = function(props) {
    return (
              <div className="jumbotron">
                <h1>Introduction à React </h1>
                <p>
                  React implémente un DOM virtuel, une représentation interne du DOM extrêmement rapide. La méthode render retourne des objets correspondant à la représentation interne du DOM virtuel.

                  Un composant React reçoit des props, ses paramètres, et peut avoir un state, un état local. Lorsque l'on définit un composant React, on peut y mettre un état par défaut en définissant une propriété state dans la class.

                  Après cela, on peut mettre à jour l'état avec la méthode this.setState, en y passage les valeurs de l'état à changer afin de mettre à jour le DOM.
                </p>
              </div>
            )
  }

export default App;
