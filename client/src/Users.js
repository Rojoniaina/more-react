import React, { Component } from 'react';

class Users extends Component {

	state = {
		users : [],
		userToUpdate  : {},
		isForm : false,
		CreateOrEdit : '',
	}

	constructor(props) {
      super(props);
      this.onButtonClick = this.onButtonClick.bind(this);
  	}

	componentDidMount(){
		const tab = [
						{
							u_id : 1,
							u_name : 'Rojoniaina',
							u_email : 'rojoniaina.teko@gmail.com',
							u_address : 'Ambohimiandra'
						},
						{
							u_id : 2,
							u_name : 'Ranto',
							u_email : 'ranto.teko@gmail.com',
							u_address : 'Ambanidia'
						},
						{
							u_id : 3,
							u_name : 'Nasandratra',
							u_email : 'nasandratra.teko@gmail.com',
							u_address : 'Ivory Atsimo'
						},
					];
		this.setState({users : tab});
	}

	onButtonClick(evt) {
      evt.preventDefault();
      const target = evt.target;
      const action = target.action;
      const id  = target.value;

      console.log(action);
      
      if(action === "delete") 
      {
      	this.setState({ users :this.state.users.filter(us => us.u_id !== id) });
      } 
      else 
      {
	 	this.state.users.map((user) => {
			if (user.u_id === parseInt(id)) {
				this.setState({isForm : true, CreateOrEdit : action,userToUpdate : user});
			}
			return user.u_id;
		});
	  }
  	}

	render() {
	      if(this.state.isForm === true) {
	      	return(<UserForm user = {this.state.userToUpdate} />);
	      } else {
	      	return(<UsersList 
	      					users = {this.state.users}
	      					onButtonClick = {this.onButtonClick}
	      		  />);
	      }
    }
}

const UsersList = function(props) {
	return (
		<div>
			<div className="row">
			  <div className="col-md-6 col-md-offset-3">
			  		<button type="button" className="btn btn-primary btn-lg btn-block"
			  				action="create"
							value=''
							onClick={props.onButtonClick}>Créer un utilisateur</button>
			  </div>
			</div>
			<br />
			<div className="col-md-12">
		          <table className="table table-striped">
		            <thead>
		              <tr>
		                <th>#ID</th>
		                <th>Name</th>
		                <th>Email</th>
		                <th>Adresse</th>
		                <th>Actions</th>
		              </tr>
		            </thead>
		            <tbody>
		            {props.users.map(
		            	(user) =>  	(<tr key={user.u_id}>
						                <td>{user.u_id}</td>
						                <td>{user.u_name}</td>
						                <td>{user.u_email}</td>
						                <td>{user.u_address}</td>
						                <td>
						                <div className="row">
						                	<div className="col-sm-2">
												<button type="button" className="btn btn-default btn-xs" 
														action="edit"
														value={user.u_id}
														onClick={props.onButtonClick}>
													<span className="glyphicon glyphicon-edit"></span>
												</button>
											</div>
											<div className="col-sm-2">
												<button type="button" className="btn btn-danger btn-xs"
														action="delete"
														value={user.u_id}
														onClick={props.onButtonClick}
														>
													<span className="glyphicon glyphicon-remove"></span>
												</button>
											</div>
										</div>
						                </td>
						             </tr>)

		            )}
		            </tbody>
		          </table>
		        </div>
		    </div>
			);
}

const UserForm = function(props) {
    return (
              <div className="jumbotron">
               	<form className="form-horizontal">
				  <div className="form-group">
				    <label className="col-sm-3 control-label">Nom</label>
				    <div className="col-sm-8">
				      <input type="text" className="form-control" id="inputName" placeholder='Nom' defaultValue={props.user.u_name}></input>
				    </div>
				  </div>
				  <div className="form-group">
				    <label className="col-sm-3 control-label">Email</label>
				    <div className="col-sm-8">
				      <input type="email" className="form-control" id="inputEmail3" placeholder="Email" defaultValue={props.user.u_email}></input>
				    </div>
				  </div>
				  <div className="form-group">
				    <label className="col-sm-3 control-label">Password</label>
				    <div className="col-sm-8">
				      <input type="text" className="form-control" id="inputAdresse" placeholder="Adresse" defaultValue={props.user.u_address}></input>
				    </div>
				  </div>
				  <div className="form-group">
				  	<label className="col-sm-3 control-label">&nbsp;</label>
				  	<div className="row">
					    <div className="col-sm-1">
					      <button type="submit" className="btn btn-primary btn-sm">Valider</button>
					    </div>
					    <div className="col-sm-2">
					      <button type="submit" className="btn btn-danger btn-sm">Annuler</button>
					    </div>
					</div>
				  </div>
				</form>
              </div>
            )
  }

export default Users;