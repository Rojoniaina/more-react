import React, { Component } from 'react';

class Taskes extends Component {

	render() {
	      return (
	        
	        <div className="col-md-12">
	          <table className="table table-striped">
	            <thead>
	              <tr>
	                <th>#ID</th>
	                <th>First Name</th>
	                <th>Last Name</th>
	                <th>Username</th>
	              </tr>
	            </thead>
	            <tbody>
	              <tr>
	                <td>1</td>
	                <td>Mark</td>
	                <td>Otto</td>
	                <td>@mdo</td>
	              </tr>
	            </tbody>
	          </table>
	        </div>
	       
	      );
    }
}

export default Taskes;